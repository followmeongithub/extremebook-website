---
title: Finding the Right Base Hardware
date: '2023-08-30 20:00:00'
categories:
- sales
author_staff_member: ed
---

After setting the objective to build a Mobile Modular Cluster Computing platform and establishing Pelican cases as the ExtremeBook shell, we needed to start filling it with hardware!

Before starting to throw any old piece of hardware in the case, we needed to define what success looked like. As we began defining criteria, we selected a "standard" to measure against. The standard wasn't the latest generation, but it was still a satisfying "daily-driver".

![ODROID-C4](/images/parts/Lenovo-W540.jpeg)

The specifications of our "standard" are not impressive by today's standards, but it was capable enough to act as a baseline for the ExtremeBook.

![ODROID-C4](/images/parts/thinkpad_specs.png)

<hr>
<hr>

## Key Requirements
We broke the basic criteria down into two categories.

**Performance:**
- Smooth HD playback
	- 4K nature documentary played via VLC
- Web browsing is smooth
- Battery life is >2hrs

**Features:**
- Components can upgraded or changed individually (i.e. modular)

The performance requirements are really laid out as test criteria. That is to say that we set capability targets while being careful not to put too many assumptions on what tehcnical specifications that may imply, yet.

## Mainboard Requirements
Once the high-level requirements were established we turned our focus to the Mainboard. Here we begin to get a little more specific.


**Minimum Specifications:**
- <20W peak power
- Smaller than 6" x 6" footprint
- Quad-core CPU
- Enough memory for multi-tasking*


*At least 4GB, but ideally more than 16GB



You may be noting that the mainboard requirements *likely* satisfy the performance criteria laid out under Key Requirements. Even though our base unit would include both a **Mainboard** and a **Node1**,  clustering was being designed as an advanced capability and not a performance crutch. We wanted our mainboard *alone* to be capable of "daily-driving".


## Selecting a Mainboard: Meet The Contenders
We did our research and pulled together some of the best SBCs the industry had to offer.

[>>SKIP AHEAD TO THE RESULTS](#selecting-a-mainboard-results)

<hr>
### ODROID-C4

![ODROID-C4](/images/parts/ODROID-C4.png)
![ODROID-C4 Specs](/images/parts/ODROID-C4_specs.png)

<hr>

### ODROID-N2

![ODROID-C4](/images/parts/ODROID-N2.png)
![ODROID-C4 Specs](/images/parts/ODROID-N2_specs.png)

<hr>


### ODROID-M1

![ODROID-C4](/images/parts/ODROID-M1.png)
![ODROID-C4 Specs](/images/parts/ODROID-M1_specs.png)

<hr>

### Raspberry Pi 4

![ODROID-C4](/images/parts/RaspberryPi-4.png)
![ODROID-C4 Specs](/images/parts/RPi4_specs.png)

<hr>

### ODROID-H3+

![ODROID-C4](/images/parts/ODROID-H3.jpg)
![ODROID-C4 Specs](/images/parts/ODROID-H3_specs.png)

<hr>




## Selecting a Mainboard: Results


<table border="1" style="background-color:FFFFCC;border-collapse:collapse;border:1px solid FFCC00;color:000000;width:100%" cellpadding="3" cellspacing="3">
	<tr>
		<td>Requirement</td>
		<td>ODROID-C4</td>
		<td>ODROID-N2</td>
		<td>ODROID-M1</td>
		<td>Raspberry Pi 4</td>
		<td>ODROID-H3+</td>
	</tr>
	<tr>
		<td>HD Playback</td>
		<td>Poor</td>
		<td>Moderate</td>
		<td>Poor</td>
		<td style="background-color:#4182e4">Moderate</td>
		<td style="background-color:#8dcf3f">Good</td>
	</tr>
		<tr>
		<td>Smooth Browsing</td>
		<td>Poor</td>
		<td>Moderate</td>
		<td>Poor</td>
		<td style="background-color:#4182e4">Moderate</td>
		<td style="background-color:#8dcf3f">Good</td>
	</tr>
		<tr>
		<td>Available Interfaces</td>
		<td>Good</td>
		<td>Good</td>
		<td>Good</td>
		<td style="background-color:#4182e4">Good</td>
		<td style="background-color:#8dcf3f">Great</td>
	</tr>
</table>

	
While we remain fond of all the boards tested, there was no question which board would become our new Mainboard. The **ODROID-H3+** outpaced every other board by a decent margin. In addition to checking off all of our basic requirements, the ODROID-H3+ brought additional capabilities such as multiple Ethernet ports. These will come in handy for clustering!

These tests also showed that the Raspberry Pi 4 (8GB) sat in second place. The ODROID-M1 was not far behind and even had some advantages over the Raspberry Pi 4, but the M1 couldn't compete on video performance - and it was never intended to.


With these results determining both the Mainboard and Node1, our challenge now was to build a power system (batteries, charger, voltage converters) that would support both of these boards and a 4K mobile screen. Our next post will dive into charging protocols and the details behind the dual-battery system that powers the ExtremeBook.
