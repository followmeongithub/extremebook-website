---
date: 2023-06-21
title: Extreme Inspiration - What Makes Our Book Extreme
categories:
  - sales
author_staff_member: ed
---

In the 1990's and 2000's, desktop computers largely ruled the technology landscape. 
What they lacked in portability, they made up for with basic modular design. 

Peripherals like monitors, mice, and keyboards were each replaceable of course, but it was much more than that. 
Power supplies, disk drives, memory, CPUs, and even motherboards were common replacable items.

This paradigm largely shifted with the uptake of laptops and smartphones.

![Broken Laptop](https://images.unsplash.com/photo-1583413230540-ddf9068c9d2d)


## Repairing Modern Devices

Early generations of laptops and smartphones exhibited some modularity.
Laptops could easily eject their batteries for swapping or replacement.
CD/DVD drives, WiFi devices, and various other devices that attached via card interfaces and the like were easily replaceable and standard.
Smartphones were mostly limited to removable batteries; an option many manufacturers have now moved away from.

![Repairing a Device](https://images.unsplash.com/photo-1576613109753-27804de2cba8)

Modern devices are still repairable, but it often requires special tools and knowledge. 
Replacing the screen of a smartphone or de-soldering the RAM on a motherboard is not for the faint of heart.

We wanted something more than just a device; we wanted a mobile, modular platform.
A platform built on standard interfaces and common hardware.
Something that can be easily repaired and upgraded. So we set off to build just that!

## Building a Mobile Modular Platform

While we were eager to dive into some more of the technical details when beginning this build, it was important that if the platform was ever to be mobile, we would need to start with a case.

Building a custom case was tempting but would bring its own challenges. 
After reviewing several different creative case ideas, we determined Pelican cases were a perfect match for the ExtremeBook.
Not only are Pelican cases rugged, but they come in various shapes and sizes and are widely available both new and used.

After selecting the case, it was time to begin measuring and testing hardware.
Once we determined what boards would make up our core unit, the hunt was on for an adequate power solution to support the platform.

We will post more articles in the future detailing how we selected hardware and the journey to power it up.

![Modular Device](https://images.unsplash.com/photo-1603732551658-5fabbafa84eb)
